module Timer (
  Timers
  , Timer(..)
  , runTimers
  , runTimer
  )
where

import Control.Concurrent.Timeout
import Control.Monad
import Data.Duration()

type Timers = [Timer]
data Timer = Timer {
  tDuration :: Duration
  , tAction :: IO ()
  }

runTimer :: Timer -> IO ()
runTimer (Timer duration action) = threadDelay duration >> action

runTimers :: Timers -> IO ()
runTimers = forever . mapM_ runTimer

