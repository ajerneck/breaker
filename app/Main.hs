{-# LANGUAGE OverloadedStrings #-}

module Main where

import Timer
import Data.Duration
import Data.Version (showVersion)
import Paths_breaker (version)
import System.Process.Typed
import System.Environment
import System.Exit

-- TODO: the logic is wrong, it should do the action first, then wait.
  -- now the logic is that first it waits for work starts, then, waits for the break, then does the xmessage which times out after another duration. So the xmessage timeout is the break, the duration for the break timer is just added on to the time for work starts. 

loadTimers :: Int -> Int -> Timers
loadTimers w b = [
  Timer (minutes w) (putStrLn "Work Starts")
  , Timer (seconds 1) (showMessage "Break Starts" (Just $ minutes b ))
  ]

toSeconds :: Duration -> Int
toSeconds x = div (durationUs x) 1000000

showMessage :: String -> Maybe Duration -> IO ()
showMessage msg timeout = do
  let args = case timeout of
        Just t ->  unwords ["xmessage", msg, "-timeout", show $ toSeconds t, "-default okay"]
        Nothing -> unwords ["xmessage", msg]

  putStrLn args

  runProcess_ (shell args)

parseDuration :: String -> Int
parseDuration = read

main :: IO ()
main = do
  args <- getArgs

  case args of
    ["--version"] -> do
      putStrLn $ "breaker " <> showVersion version
      exitSuccess
    _ -> pure ()

  let timers = case args of
               [w, b] -> loadTimers (parseDuration w) (parseDuration b)
               _ -> loadTimers 25 2

  putStrLn "Start of program"
  print $ map tDuration timers

  _ <- runTimers timers

  putStrLn "End of program"
